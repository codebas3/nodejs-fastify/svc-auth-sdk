# web-server
> Used for SSR Web App or Backend API

## Installation

Make sure you set `AUTH_ENV` variable, correspondency with `Auth server` environment used, this will route pulled `Public Key`, that should be paired with `Private Key` in `Auth server`. See [certificate directory](../certificate/) for available `env`.
```
# It's can be development, stagging, or production. Default: local
export AUTH_ENV=production
```

Node
```bash
npm install git+https://edot.ro:SaJfZeAyeqQLFWNfbasp@gitlab.edot.id/edot/core-api/sso/svc-auth-sdk.git#main
```

Golang
```bash
echo WIP
```

## Usage

While `middleware.authentication(<bearerToken>)` turn an error, the `message` available is:
1. `jwt must be provided`: Caused a empty `<bearerToken>`
2. `invalid signature`: Public key is invalid
3. `invalid algorithm`: Auth server and this library using different algorithm
4. `invalid token`: Bearer Token format is wrong
5. `jwt expired`: JWT are expired
> If you are using `Node` it can be found on `error.message` variable. Visit [src/middleware/authentication.test.js](./src/middleware/authentication.test.js) for and error examples.

### Node

```js
const express = require('express')
const { middleware: { authentication} } = require('svc-auth-sdk')

const app = express()

const handler = (req, res) => res.send({ 'decoded': req.usr })
app.get('/protected/endpoint', authentication.express, handler)

// express error handler
app.use((err, req, res, next) => {
  res.status(500).send({
    code: err.code,
    error: err.message,
    stack: err.stack.split('\n')
  })
})

app.listen(3000)
```

### Golang
```js
# WIP
```

Test endpoint using curl
```bash
curl --location 'http://localhost:3000/protected/endpoint' \
  --header 'Accept: application/json' \
  --header 'Authorization: Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6ImFlZjE4ZjM3LTA1NGEtNDRlNy04NDgxLTM2NDgxYTUwZGE1YyJ9.eyJzdWIiOiJtaWNyb3Nwb25penppZWRAeWFob28uY29tIiwiYmlydGhkYXRlIjoiMTk4Ny0xMC0xNiIsImZhbWlseV9uYW1lIjoiRG9lIiwiZ2VuZGVyIjoibWFsZSIsImdpdmVuX25hbWUiOiJKb2huIiwibG9jYWxlIjoiZW4tVVMiLCJtaWRkbGVfbmFtZSI6Ik1pZGRsZSIsIm5hbWUiOiJKb2huIERvZSIsIm5pY2tuYW1lIjoiSm9obnkiLCJwaWN0dXJlIjoiaHR0cDovL2xvcmVtcGl4ZWwuY29tLzQwMC8yMDAvIiwicHJlZmVycmVkX3VzZXJuYW1lIjoiam9obm55IiwicHJvZmlsZSI6Imh0dHBzOi8vam9obnN3ZWJzaXRlLmNvbSIsInVwZGF0ZWRfYXQiOjE0NTQ3MDQ5NDYsIndlYnNpdGUiOiJodHRwOi8vZXhhbXBsZS5jb20iLCJ6b25laW5mbyI6IkV1cm9wZS9CZXJsaW4iLCJhdF9oYXNoIjoiZ3d1MmV6YXNDLW5TN1BZRU9GWnVBUSIsImF1ZCI6ImFpby1jbGllbnQiLCJleHAiOjE2ODUxODYyMjcsImlhdCI6MTY4NTE4MjYyNywiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDozMDAwIn0.DP1yNlOaNPUupZmxmPiRlD9rbN5l3YeycLb-xPHxBaWDhc_CArv1VS4hEQAhKOpk16UtDCHD1KYOpLW450C_pG2TxEPREYIvUeaeMU8VOxc3u2_6yLa7ArluqF3-lgWvKB_O-zfLVGX9Qpy1wKsuHWNcNurFs6I0UaPH2zfooft_ikkk9hBFpQ0K3BRUFp07QdUmffqIOKZWwk2lTKUOQIeljgsZGQzL9DxeIfPTco1q-AAe5le3PcW4pCMyH0lAqO58zoPsjfmccnsCtzL9ExwgasfqwoZbXQ3OPGcDsdu7Lrc5DPO_E4xst2A0eAUjtbBq5bbQ8g-bx3Q51fkm0g'
```

