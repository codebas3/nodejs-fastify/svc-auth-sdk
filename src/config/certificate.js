/* eslint-disable no-console */

require('dotenv').config()

const { env } = process
const { join } = require('path')
const { existsSync, readFileSync } = require('fs')

const getPrefix = () => {
  let prefix = 'local.';
  if (['dev', 'development'].indexOf(env.AUTH_ENV) > -1) prefix = 'dev.'
  else if (['stag', 'stagging'].indexOf(env.AUTH_ENV) > -1) prefix = 'stag.'
  else if (['prod', 'production'].indexOf(env.AUTH_ENV) > -1) prefix = ''

  return prefix
}

const algorithm = 'RS256'
const dir = join(__dirname, '..', '..', 'certificate');

/** Private certificate used for signing JSON WebTokens */
console.debug(`Using AUTH_PRIVATE_KEY or "./${join('certificate', `${getPrefix()}private.key`)}"`)
const PRIVATE_KEY = env.AUTH_PRIVATE_KEY || join(dir, `${getPrefix()}private.key`)
const SECRET = existsSync(PRIVATE_KEY) ? readFileSync(PRIVATE_KEY) : PRIVATE_KEY;

/** Private certificate used for signing JSON WebTokens */
console.debug(`Using AUTH_PRIVATE_PKCS8_KEY or "./${join('certificate', `${getPrefix()}private.pkcs8.key`)}"`)
const PRIVATE_PKCS8_KEY = env.AUTH_PRIVATE_PKCS8_KEY || join(dir, `${getPrefix()}private.pkcs8.key`)
const SECRET_PKCS8 = existsSync(PRIVATE_PKCS8_KEY) ? readFileSync(PRIVATE_PKCS8_KEY) : PRIVATE_PKCS8_KEY;

/** Public certificate used for verification.  Note: you could also use the private key */
console.debug(`Using AUTH_PUBLIC_KEY or "./${join('certificate', `${getPrefix()}public.key`)}"`)
const PUBLIC_KEY = env.AUTH_PUBLIC_KEY || join(dir, `${getPrefix()}public.key`)
const PUBLIC = existsSync(PUBLIC_KEY) ? readFileSync(PUBLIC_KEY) : PUBLIC_KEY;

module.exports = {
  getPrefix,
  public: PUBLIC === PUBLIC_KEY ? undefined : PUBLIC,
  secret: SECRET === PRIVATE_KEY ? undefined : SECRET,
  secretPkcs8: SECRET_PKCS8 === PRIVATE_PKCS8_KEY ? undefined : SECRET_PKCS8,
  algorithm,
  verifyOptions: { algorithms: [algorithm] }
}
