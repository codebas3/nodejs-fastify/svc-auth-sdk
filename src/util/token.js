const jwt = require('jsonwebtoken')
const { certificate: { secret, algorithm, public: pub } } = require('../config')

async function generate(data) {
  return jwt.sign(data, secret, { algorithm })
}

async function decode(token) {
  return jwt.verify(token, pub || secret, { algorithm })
}

module.exports = { generate, decode };
