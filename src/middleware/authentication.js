const { token: Token } = require('../util');

const validate = async (bearerToken) => {
  const tokens = (bearerToken || '').split(/\s/g)
  const token = tokens[tokens.length === 1 ? 0 : 1]

  return Token.decode(token)
}

const express = async (req, res, next) => {
  const bearerToken = req.headers.authorization;
  try {
    req.usr = await validate(bearerToken)
    next()
  } catch (e) {
    next(e)
  }
}

module.exports = { validate, express }
