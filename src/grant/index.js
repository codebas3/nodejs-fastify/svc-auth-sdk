module.exports = {
  authorizationCode: require('./authorization-code'),
  password: require('./password'),
  clientCredential: require('./client-credential'),
  refreshToken: require('./refresh-token'),
}
