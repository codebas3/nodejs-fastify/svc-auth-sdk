/* eslint-disable camelcase, default-param-last, no-useless-escape */

require('dotenv').load()

const {
  AUTH_URL,
  AUTH_CLIENT_ID,
  AUTH_CLIENT_CALLBACK_URI,
  AUTH_CLIENT_SCOPE
} = process.env

const jsonToQueryString = (params) => Object.keys(params)
  .map((key) => `${encodeURIComponent(key)}=${encodeURIComponent(params[key])}`)
  .join('&')

const login = async (
  oidcHost = AUTH_URL,
  {
    client_id = AUTH_CLIENT_ID,
    resource,
    redirect_uri = AUTH_CLIENT_CALLBACK_URI,
    scope = AUTH_CLIENT_SCOPE
  }
) => {
  const textEncoder = new TextEncoder()

  function randomString(length) {
    let result = ''
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
    const charactersLength = characters.length
    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength))
    }
    return result
  }

  async function sha256(str) {
    if (!crypto.subtle) console.error('Crypto module only run over HTTPS or localhost!')
    return new Uint8Array(await crypto.subtle.digest('SHA-256', textEncoder.encode(str)))
  }

  function toHex(uint8Array) {
    const hashArray = Array.from(uint8Array)
    const hashHex = hashArray.map((b) => b.toString(16).padStart(2, '0')).join('')
    return hashHex
  }

  function hexToBase64(str) {
    return btoa(
      String.fromCharCode.apply(
        null,
        str
          .replace(/\r|\n/g, '')
          .replace(/([\da-fA-F]{2}) ?/g, '0x$1 ')
          .replace(/ +$/, '')
          .split(' ')
      )
    )
  }
  const codeVerifier = randomString(64)

  // Browser dependency!
  window.sessionStorage.setItem('codeVerifier', codeVerifier)

  const code_challenge = hexToBase64(toHex(await sha256(codeVerifier)))
    .replace(/[\+]/g, '-')
    .replace(/[\/]/g, '_')
    .replace(/[=]/g, '')

  const obj = {
    client_id,
    grant_type: 'authorization_code',
    response_type: 'code',
    redirect_uri,
    scope,
    prompt: 'consent',
    code_challenge,
    code_challenge_method: 'S256',
  }
  if (resource) obj.resource = resource

  // Browser dependency!
  window.location.href = `${oidcHost}/auth?${jsonToQueryString(obj)}`
}

module.exports = {
  login
}
