const process = window;

process.env = {
  AUTH_URL: 'http://localhost:3000/oidc',
  AUTH_CLIENT_ID: 'aio-client',
  AUTH_CLIENT_SECRET: 'aio-client',
  AUTH_CLIENT_CALLBACK_URI: 'http://localhost:3030/callback.html',
  AUTH_CLIENT_SCOPE: 'openid offline_access profile',
}
