module.exports = {
  config: require('./src/config'),
  middleware: require('./src/middleware'),
  grant: require('./src/grant'),
  util: require('./src/util')
}
